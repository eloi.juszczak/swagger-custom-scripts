const fs = require("fs");

// Authentification : enregistrement du token dans une variable d'environnement
const donneesBrutesAuthentification = fs.readFileSync(
  "./authentification/jeuDeDonnees.API DSN AUTHENTIFICATION.postman_collection.json",
  "utf8"
);
const collectionAuthentification = JSON.parse(donneesBrutesAuthentification);
collectionAuthentification.item.map((element) => {
  const scriptEnregistrementToken = {
    listen: "test",
    script: {
      exec: [
        "const apiDsnJeton = pm.response.text();\r",
        'pm.environment.set("apiDsnJeton", apiDsnJeton);',
      ],
      type: "text/javascript",
    },
  };
  if (Array.isArray(element.event)) {
    element.event.push(scriptEnregistrementToken);
  } else if (!element.event) {
    element.event = [scriptEnregistrementToken];
  } else {
    console.error(
      "Comportement inconnu, la section 'event' de la requête d'authentification a posé un soucis"
    );
    throw "Comportement inconnu, la section 'event' de la requête d'authentification a posé un soucis";
  }
});

fs.writeFile(
  "./authentification/enregistrementToken.jeuDeDonnees.API DSN AUTHENTIFICATION.postman_collection.json",
  JSON.stringify(collectionAuthentification),
  function (err) {
    if (err) {
      console.log(err);
    }
  }
);

const ajoutTokenALaCollection = (cheminFichierOriginal, cheminFichierFinal) => {
  console.log(cheminFichierOriginal);
  const donneesBrutes = fs.readFileSync(cheminFichierOriginal, "utf8");
  console.log("donneesBrutes", donneesBrutes);
  const collection = JSON.parse(donneesBrutes);
  if (collection.auth.type != "bearer") {
    console.error(
      "le header Authorization n'est pas correctement configuré. Regardez au niveau de la collection que l'onglet 'Authorization' est bien rempli. Regardez églament du côté d'openapi.yaml, si l'attribut securityScheme est bien configuré : \n cf. https://swagger.io/docs/specification/authentication/ \n https://swagger.io/docs/specification/authentication/bearer-authentication/ "
    );
    throw "le header Authorization n'est pas correctement configuré. Regardez au niveau de la collection que l'onglet 'Authorization' est bien rempli. Regardez églament du côté d'openapi.yaml, si l'attribut securityScheme est bien configuré : \n cf. https://swagger.io/docs/specification/authentication/ \n https://swagger.io/docs/specification/authentication/bearer-authentication/ ";
  }
  collection.auth.bearer[0].value = "DSNLogin jeton={{apiDsnJeton}}";
  fs.writeFile(cheminFichierFinal, JSON.stringify(collection), function (err) {
    if (err) {
      console.log(err);
    }
  });
};

// Depot
ajoutTokenALaCollection(
  "./depotDsn/API DSN DEPOT DSN.postman_collection.json",
  "./depotDsn/variabilisationToken.API DSN DEPOT DSN.postman_collection.json"
);

// Consultation Dépôts et retours
ajoutTokenALaCollection(
  "./consultationDepotsRetours/API DSN CONSULTATION DEPOTS RETOURS.postman_collection.json",
  "./consultationDepotsRetours/variabilisationToken.API DSN CONSULTATION DEPOTS RETOURS.postman_collection.json"
);

// Consultation fices de paramétrage
//ajoutTokenALaCollection(
//  "./consultationDepotsRetours/API DSN CONSULTATION FICHES PARAMETRAGE.postman_collection.json",
//  "./consultationDepotsRetours/variabilisationToken.API DSN CONSULTATION FICHES PARAMETRAGE.postman_collection.json"
//);
