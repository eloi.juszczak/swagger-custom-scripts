const fs = require("fs");
var parser = require("xml2json");

function renameKey(obj, oldKey, newKey) {
  obj[newKey] = obj[oldKey];
  delete obj[oldKey];
}

const lireFichierEtParserJson = (nomFichier) => {};

const extraireCorpsRequete = (cheminFichier) => {
  const donneesBrutes = fs.readFileSync(cheminFichier);
  const collection = JSON.parse(donneesBrutes);
  const corpsRequete = collection.item[0].request.body.raw;
  const corpsRequeteJsonString = parser.toJson(corpsRequete, {
    reversible: true,
  });
  return JSON.parse(corpsRequeteJsonString);
};

const modifierCorpsRequete = (
  cheminFichierOriginal,
  cheminFichierFinal,
  modification
) => {
  const donneesBrutes = fs.readFileSync(cheminFichierOriginal);
  const collection = JSON.parse(donneesBrutes);
  collection.item[0].request.body.raw = modification;
  fs.writeFile(cheminFichierFinal, JSON.stringify(collection), function (err) {
    if (err) {
      console.log(err);
    }
  });
};

// authentification
const authentificationJson = extraireCorpsRequete(
  "./authentification/API DSN AUTHENTIFICATION.postman_collection.json"
);

const ajoutDesVariablesDeCollectionAuthentification = (
  fichierAModifier,
  siret,
  prenom,
  nom,
  motdepasse,
  service
) => {
  const donneesBrutes = fs.readFileSync(fichierAModifier, "utf8");
  const collection = JSON.parse(donneesBrutes);
  collection.item.variable.push({ key: "siret", type: "string", value: siret });
  collection.item.variable.push({
    key: "prenom",
    type: "string",
    value: prenom,
  });
  collection.item.variable.push({ key: "nom", type: "string", value: nom });
  collection.item.variable.push({
    key: "motdepasse",
    type: "string",
    value: motdepasse,
  });
  collection.item.variable.push({
    key: "service",
    type: "integer",
    value: service,
  });
  fs.writeFile(fichierAModifier, JSON.stringify(collection), function (err) {
    if (err) {
      console.log(err);
    }
  });
};

console.log("authentificationJson : " + JSON.stringify(authentificationJson));

authentificationJson.element.siret["$t"] = "{{siret}}";
authentificationJson.element.prenom["$t"] = "{{prenom}}";
authentificationJson.element.nom["$t"] = "{{nom}}";
authentificationJson.element.motdepasse["$t"] = "{{motdepasse}}";
authentificationJson.element.service["$t"] = "{{service}}";
renameKey(authentificationJson, "element", "identifiants");

const xml = parser.toXml(JSON.stringify(authentificationJson));
modifierCorpsRequete(
  "./authentification/API DSN AUTHENTIFICATION.postman_collection.json",
  "./authentification/jeuDeDonnees.API DSN AUTHENTIFICATION.postman_collection.json",
  xml
);

ajoutDesVariablesDeCollectionAuthentification(
  "./authentification/jeuDeDonnees.API DSN AUTHENTIFICATION.postman_collection.json",
  "99900000300015",
  "declarant",
  "declarant",
  "Refonte01",
  25
);
